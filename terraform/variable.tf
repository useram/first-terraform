variable "region" {
  type        = string
  description = "AWS Region the instance is launched in"
  default     = "ap-south-1"
}

variable "vpc_cidr" {
  default     = "10.0.0.0/16"
  description = "VPC CIDR Block"
  type        = string
}

variable "publicsubnet1_cidr" {
  default     = "10.0.0.0/24"
  description = "PublicSubnet CIDR Range"
  type        = string
}

variable "publicsubnet2_cidr" {
  default     = "10.0.1.0/24"
  description = "Privatesubnet CIDR Range"
  type        = string
}

variable "privatesubnet1_cidr" {
  default     = "10.0.2.0/24"
  description = "Privatesubnet CIDR Range"
  type        = string
}

variable "privatesubnet2_cidr" {
  default     = "10.0.3.0/24"
  description = "Privatesubnet CIDR Range"
  type        = string
}
