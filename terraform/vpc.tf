resource "aws_vpc" "test_vpc" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = "true"
  enable_dns_support   = "true"

  tags = {
    Name = "Test VPC"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.test_vpc.id

  tags = {
    "Name" = "Test IGW"
  }
}

resource "aws_subnet" "public_subnet-1" {
  vpc_id                  = aws_vpc.test_vpc.id
  cidr_block              = var.publicsubnet1_cidr
  availability_zone       = "ap-south-1a"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "PublicSubnet-1"
  }
}

resource "aws_subnet" "public_subnet-2" {
  vpc_id                  = aws_vpc.test_vpc.id
  cidr_block              = var.publicsubnet2_cidr
  availability_zone       = "ap-south-1b"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "PublicSubnet-2"
  }
}

resource "aws_subnet" "private_subnet-1" {
  vpc_id                  = aws_vpc.test_vpc.id
  cidr_block              = var.privatesubnet1_cidr
  availability_zone       = "ap-south-1a"
  map_public_ip_on_launch = "false"

  tags = {
    Name = "PrivateSubnet-1"
  }
}

resource "aws_subnet" "private_subnet-2" {
  vpc_id                  = aws_vpc.test_vpc.id
  cidr_block              = var.privatesubnet2_cidr
  availability_zone       = "ap-south-1a"
  map_public_ip_on_launch = "false"

  tags = {
    Name = "PrivateSubnet-2"
  }
}

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.test_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "Public_Route"
  }
}

resource "aws_route_table_association" "public_subnet-1_route_table_association" {
  subnet_id      = aws_subnet.public_subnet-1.id
  route_table_id = aws_route_table.public_route_table.id

}

resource "aws_route_table_association" "public_subnet-2_route_table_association" {
  subnet_id      = aws_subnet.public_subnet-2.id
  route_table_id = aws_route_table.public_route_table.id

}
